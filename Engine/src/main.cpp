#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#define MAIN_CODE 0

#if MAIN_CODE == 1




enum exitCodes { ENGINE_OK = 0, INIT_ERROR, WINDOW_NOT_CREATED };

#define ECHO_DEBUG_LOG 1

#if ECHO_DEBUG_LOG == 1
#define LOG(X) std::cout << X << '\n'
#define IFLOG(STATE,MESSAGE) if (STATE) LOG(MESSAGE)
#else
#define LOG(X)
#endif

#define LOG_MESSAGE_LEN 1024
#define FULLSCREEN 0
#define S_WIDTH 640
#define S_HEIGHT 480

unsigned int s_height = S_HEIGHT;
unsigned int s_width = S_WIDTH;

// TODO: error checking log
static void GLGetError()
{
	while (glGetError() != GL_NO_ERROR)
	{
	}
}

static void GLCheckError()
{
	while (GLenum error = glGetError() )
	{
		LOG(error);
	}
}



struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

// TODO: replace with C style files
static ShaderProgramSource ParseShader(const std::string& filepath)
{
	enum class ShaderType
	{
		NONE = -1, VERTEX = 0, FRAGMENT = 1
	};

	std::ifstream stream(filepath);

	std::string line;
	std::stringstream ss[2];

	ShaderType type = ShaderType::NONE;
	while (getline(stream, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
				type = ShaderType::VERTEX;
			else if (line.find("fragment") != std::string::npos)
				type = ShaderType::FRAGMENT;
		}
		else
		{
			ss[(int)type] << line << '\n';
		}

	}

	return { ss[0].str(), ss[1].str() };
}


static unsigned int CompileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);

	glCompileShader(id);
	// Error handling
	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);

	if (result != GL_TRUE)
	{
		int lenght = 0;
		//get shader lenght
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &lenght);
		// dynamic allocation on stack
		GLchar message[LOG_MESSAGE_LEN];
		glGetShaderInfoLog(id, LOG_MESSAGE_LEN, &lenght, message);

		LOG("Failed to Compile: ");
		LOG((type == GL_VERTEX_SHADER ? "VERTEX " : "FRAGMENT "));
		LOG(message);

		glDeleteShader(id);

		return 0;
	}


	return id;
}

static unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader)
{
	unsigned int program = glCreateProgram();
	//vertex shader 
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	// fragment shader
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);

	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}



void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_E && (action == GLFW_REPEAT || action == GLFW_PRESS))
	{
		s_height++;
		s_width++;
		glfwSetWindowSize(window, s_width, s_height);
	}

}

void drop_callback(GLFWwindow* window, int count, const char** paths)
{
	int i;
	for (i = 0; i < count; i++)
		std::cout << paths[i] << "\n";
}

void increment_r(float &r,bool &state)
{
	if (r > 1.0 || r<0.0)
		state = !state;
	if (state)
		r += 0.01f;
	else
		r -= 0.01f;
}


#if 0
int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;



	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(S_WIDTH, S_HEIGHT, "cyka", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	//sync with monitor framerate
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
		LOG("ERROR");


	LOG(glGetString(GL_VERSION));


	float positions[] = {
		-0.5f, -0.5f,
		0.5f, -0.5f,
		0.5f,  0.5f,
		-0.5f,  0.5f
	};

	unsigned int indices[] =
	{  
		0,1,2,
		2,3,0
	};

	unsigned int buffer;
	glGenBuffers(1, &buffer);
	//Select.
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	// FILL WITH DATA.
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(float), positions, GL_STATIC_DRAW);

	//ENABLE attribute by index!
	glEnableVertexAttribArray(0);
	// set position attr (att num , num of coord for attr, type, normalise between 0..1?, offset for each vertex in buffer, offset for this attribute inside a single vertex
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);

	unsigned int ibo;
	glGenBuffers(1, &ibo);
	//Select.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	// FILL WITH DATA.
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indices, GL_STATIC_DRAW);



	//writing shaders

	ShaderProgramSource source = ParseShader("res/shaders/Basic.shader");
	

	unsigned int shader = CreateShader(source.VertexSource, source.FragmentSource);
	// use shader
	glUseProgram(shader);

	int location = glGetUniformLocation(shader, "u_Color");
	IFLOG(location == -1, "UNIFORM ERROR");
	glUniform4f(location, 0.8f, 0.3f, 0.5f, 1.0f);

	float r = 0.0f;
	bool state = 1;
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		// DRAW

		glUniform4f(location, r, 0.3f, 1.0f, 1.0f);
		glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT, nullptr);
		glClearColor(0.0f, 0.3f, 1.0f, 1.0f);
		//key event
		//glfwSetKeyCallback(window, key_callback);

		//calculate incr
		increment_r(r, state);

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}
	glDeleteProgram(shader);
	glfwTerminate();
	return 0;
}
#endif





#if 1
int main()
{
	//can add hint for window creation here

	if (glfwInit() == GL_FALSE)
		return INIT_ERROR;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWmonitor* primary_monitor = glfwGetPrimaryMonitor();
	// borderless windowed fullscreen
#if FULLSCREEN 
	//fullscreen

	const GLFWvidmode* mode = glfwGetVideoMode(primary_monitor);

	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "My Title", primary_monitor, NULL);

	//glfwSetWindowMonitor to set window/unwindow

#else
	GLFWwindow* window = glfwCreateWindow(S_WIDTH, S_HEIGHT, "My Title", NULL, NULL);
#endif

	if (window == nullptr)
	{
		glfwTerminate();
		return WINDOW_NOT_CREATED;
	}

	glfwMakeContextCurrent(window);
	
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		LOG("ERROR");
		return INIT_ERROR;
	}
		
	glfwSetKeyCallback(window, key_callback);
	
	int w_width, w_height;
	glfwGetFramebufferSize(window, &w_width, &w_height);
	glViewport(0, 0, w_width, w_height);

	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		
		
		glfwSetDropCallback(window, drop_callback);
		/* Swap front and back buffers */
		glfwSwapBuffers(window);
	

		/* Poll for and process events */
		glfwPollEvents();
	}

	
	glfwDestroyWindow(window);

	//std::cin.get();
	return ENGINE_OK;
}
#endif




#endif // !