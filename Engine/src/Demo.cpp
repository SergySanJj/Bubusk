/*
Demo main file
*/

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "Engine.h"


#if (BUB_BUILDMODE == DEMO)

int main()
{

	return 0;
}

#endif // BuildMode



