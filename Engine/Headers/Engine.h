/*
This is basic header for whole engine.
Here consts, enums etc. will be defined. Could be used in all project's branches. 
*/


#pragma once

enum BUB_MODES { DEBUG = 0, RELEASE,  DEMO, UNDEFINED };

const int BUB_BUILDMODE = DEMO;

enum BUB_EXITCODES { BUB_OK = 0, RUNTIME_ERROR, INIT_ERROR, WINDOW_NOT_CREATED };

